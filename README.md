# pdfjs-lbry

__WARNING:__ Thomas has seen this project and he says it is [NOT advised](https://www.reddit.com/r/lbry/comments/d5uxnd/let_your_users_read_pdf_ebooks_within_lbry_app_no/f0ouq7g/) to convert an ebook like this. He hopes PDF.js will be implemented within LBRY app in future. The LBRY devs had a PDF.js implementation before, but it broke with some version of Election/Chromium. But now that this content confirms that it works, they can re-implement the feature. He has opened a [GitHub issue for the implementation here](https://github.com/lbryio/lbry-desktop/issues/2903). It was a fun project, but don't use it within LBRY. They will implement PDF.js soon. So upload PDFs directly, and they will hopefully open in PDF.js in future versions.

Simple example of a PDF.js PDF viewer that lets users read PDFs right within LBRY app! ...without downloads and PDF software installed!

This is great news. Previously, you would have to download a PDF from LBRY app to read it. Furthermore, you would have to have a PDF reader installed (or use browsers) to read the PDF. Thanks to this project, you can now skip all this and let users read PDF with ease. No checking downloads and no opening files on external PDF viewers. Users can read your PDF within LBRY app. It uses PDF.js. PDF.js has thumbnails and bookmarks, just like any other PDF viewer. So enjoy!


## Usage

Follow these 5 easy steps to get your PDF on LBRY app:

1. [Download](https://gitlab.com/funapplic66/pdfjs-lbry/-/archive/master/pdfjs-lbry-master.zip) the repository then extract. Advanced users can clone the repository: `git clone https://gitlab.com/funapplic66/pdfjs-lbry.git`

2. Delete the `extras` folder. It contains some files related to the project... not important. You can delete the `.git` folder and also the `.gitignore` and `README.md` files.

3. Now replace the `pdf.pdf` with your PDF file.

4. Now pack the directory with either [lpack.bash](https://gitlab.com/funapplic66/lpack-bash) or [lbry-format](https://github.com/lbryio/lbry-format). They are pretty much the same. I used lpack.bash so I ran a command similar to this: `lpack.bash /path/to/pdfjs-lbry output.lbry`

5. Now upload the .lbry file to LBRY and done!


## Screenshots and Features

<p><center><a href="https://gitlab.com/funapplic66/pdfjs-lbry/raw/master/extras/screenshot-01.png"><img src="https://gitlab.com/funapplic66/pdfjs-lbry/raw/master/extras/screenshot-01.png" width="40%" /></a>
<br/>Test PDF showing on the Light Themed LBRY App</center></p>

<p><center><a href="https://gitlab.com/funapplic66/pdfjs-lbry/raw/master/extras/screenshot-02.png"><img src="https://gitlab.com/funapplic66/pdfjs-lbry/raw/master/extras/screenshot-02.png" width="40%" /></a>
<br/>Test PDF showing on the Dark Themed LBRY App</center></p>

<p><center><a href="https://gitlab.com/funapplic66/pdfjs-lbry/raw/master/extras/screenshot-03.png"><img src="https://gitlab.com/funapplic66/pdfjs-lbry/raw/master/extras/screenshot-03.png" width="40%" /></a>
<br/>Shows page thumbnails which makes it easier to navigate</center></p>

<p><center><a href="https://gitlab.com/funapplic66/pdfjs-lbry/raw/master/extras/screenshot-04.png"><img src="https://gitlab.com/funapplic66/pdfjs-lbry/raw/master/extras/screenshot-04.png" width="40%" /></a>
<br/>Shows bookmarks, which in this case showing the headings in the document</center></p>

<p><center><a href="https://gitlab.com/funapplic66/pdfjs-lbry/raw/master/extras/screenshot-05.png"><img src="https://gitlab.com/funapplic66/pdfjs-lbry/raw/master/extras/screenshot-05.png" width="40%" /></a>
<br/>Has Find in document feature which is essential for a large ebook. Also has zoom and page navigation features</center></p>

<p><center><a href="https://gitlab.com/funapplic66/pdfjs-lbry/raw/master/extras/screenshot-06.png"><img src="https://gitlab.com/funapplic66/pdfjs-lbry/raw/master/extras/screenshot-06.png" width="40%" /></a>
<br/>Print, open, selection, rotation, scrolling and document info features</center></p>


## Example

I have created an example upload here: `lbry://pdfjstest#2` or [https://open.lbry.com/pdfjstest:2](https://open.lbry.com/pdfjstest:2)


## Known Issues

__When I open my upload on the LBRY web version (beta.lbry.tv) it shows "Unsupported File... Good news, though! You can Download the app and gain access to everything."__

This is because currently the web version does not support viewing .lbry packages. But LBRY developers are trying to implement this feature. So you will have to see it on the LBRY app until this feature is implemented or provide alternative download.


## License

PDF.js is licensed Apache 2.0. Some images in the test PDF are taken from [Unsplash](https://unsplash.com) which are CC0.
